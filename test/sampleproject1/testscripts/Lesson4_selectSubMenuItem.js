describe('Flipkart', function(){
    //test case for checking sub-menu item on mouseclick of menu in flipkart website
    it('to select sub-menu item', function(){
        browser.url('https://www.flipkart.com/');
        var isAppliancesElePresent = browser.isExisting('//a[@title="Appliances"]'); // check if Appliances menu element is present
        assert.equal(isAppliancesElePresent,true);
        if(isAppliancesElePresent){
            browser.click('//a[@title="Appliances"]'); // click on Applianaces
            var isRefrigeratorElePresent = browser.isExisting('//a[@title="Refrigerators"]');// check if Refrigerators element is present within Appliances
           if(isRefrigeratorElePresent){ 
               browser.click('//a[@title="Refrigerators"]'); //click on refrigerators
               var title = browser.getTitle(); 
               assert.equal(title,"Refrigerator | Buy Refrigerators Online at Best Price in India - Flipkart.com");
           }
        }
    });
});
