describe('Open New Window', function(){
    it('search for a word and check result count and invoke search result in new tab',function(){
        browser.url('http://google.com/');
        browser.setValue('//input[@title="Search"]','valuelabs');
        browser.keys("Enter");
        //check total search result count
        var resultCount = browser.getText("#resultStats");
        assert.equal(resultCount.includes("1,92,000"),true);
        //open a result in new tab
        var url = browser.getAttribute("//a[contains(.,'ValueLabs | Consulting | IT Services | Product Development')]","href");
        browser.newWindow(url);
        var searchResultUrlTitle = browser.getTitle();
        assert.equal(searchResultUrlTitle,"ValueLabs | Consulting | IT Services | Product Development");
   });
});