describe('Flipkart', function() {
    it('should have to pass the search term in search engine', function () {
        browser.url('https://www.flipkart.com/');
		browser.setValue('//input[@title="Search for Products, Brands and More"]', 'iphone6');
		browser.click("//button[@type='submit']");
       // browser.pause(10000);

        browser.waitUntil(function () {
            return browser.getTitle() === 'iphone6 - Buy Products Online at Best Price in India | Flipkart.com'
          });

        var isElePresent = browser.isExisting("//div[contains(.,'Apple iPhone 6 (Silver, 16 GB)')]");
        assert.equal(isElePresent,false);

        // verify whether the results are present after search
        // Verify IPhone 6 silver 16 GB is present on the page
        // verify the search is same as the input
    });
});


// Time Synchronization
// Dynamic 