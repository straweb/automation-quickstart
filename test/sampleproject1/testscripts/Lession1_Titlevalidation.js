var assert = require('assert');
describe('Page Title', function() {
    it('should have the right title - the fancy generator way', function () {
		browser.pause(2000);
        browser.url('http://www.cricinfo.com');
        var title = browser.getTitle();
        assert.equal(title, 'Live cricket scores, commentary, match coverage | Cricket news, statistics | ESPN Cricinfo');

    });
});

