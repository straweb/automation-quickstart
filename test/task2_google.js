var assert = require('assert');
describe('Google', function() {
    it('Search for a result in Google.com and verify no of results, search term in results if any strickout text present, open a result in new window or tab., ', function () {
        browser.url('https://www.google.co.in/');
       //Google to search for 'webdriverio current price of gold'
        browser.setValue("//input[@id='lst-ib']","webdriverio current price of gold");
        browser.keys('\uE007');
        //Getting the number of results
        var val = browser.getText("//div[@id='resultStats']");
        console.log(val);
        var a=val.split(" ");
        //Printing the number of results
        console.log(a[1]);
        var myElement = browser.element(".//div[@class='_Tib']").$('..');
        var b = myElement.getText().split("\n");
        console.log(b[0]);
        browser.newWindow(b[0]);
        browser.pause(1000);
    });
});